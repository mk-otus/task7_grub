# Otus_task7
Первая таска по смене пароля рута



```
A метод:

Grub> 'e'
Ищем строку linux16 > Ctrl + e
rw init=/bin/sh (Удалим ro rhgb и quiet ) > ctrl+x
/usr/sbin/load_policy -i
passwd root или же passwd

B метод:

Grub> 'e'
Ищем строку linux16 > Ctrl + e
rd.break or dr.break enforcing=0 (отколючаем selinux) > ctrl+x 
mount -o remount,rw /sysroot/
chroot /sysroot/
passwd root или же passwd

```

Вторая таска с rename vg

```
vgs
 VG #PV #LV #SN Attr VSize VFree
 centos 1 2 0 wz--n- <39.51g 0
 
vgrename centos otusroot
  Volume group "centos" succesfully rename to "otusroot"
 
Обновим атрибуты логическим томов
vgchange -ay
lvchange /dev/mapper/otusroot-root --refresh
lvchange /dev/mapper/otusroot-swap --refresh 
 
 
Меняем centos на otusroot

 sed -e 's/centos/otusroot/g' /etc/fstab /etc/default/grub /boot/grub2/grub.cfg
 
И дальше записываем новый initramfs
 mkinitrd -f -v /boot/initramfs-$(uname -r).img $(uname -r)
 or
 dracut -f -v
 
```

Третья таска с простейшим dracut модулем


```
mkdir /usr/lib/dracut/modules.d/01test

В нее поместим два скрипта:
1. module-setup.sh - который устанавливает модуль и вызывает скрипт test.sh
2. test.sh - собственно сам вызываемый скрипт, в нём у нас рисуется пингвинчик

mkinitrd -f -v /boot/initramfs-$(uname -r).img $(uname -r)

lsinitrd -m /boot/initramfs-$(uname -r).img | grep test
test


При загрузке системы видим

Hello! You are in dracut module!
 ___________________
< I'm dracut module >
 -------------------
   \
    \
        .--.
       |o_o |
       |:_/ |
      //   \ \
     (|     | )
    /'\_   _/`\
    \___)=(___/
